import {
  Document, Page, PDFViewer, Text, View, StyleSheet, Image,
} from '@react-pdf/renderer';
import { useSelector } from 'react-redux';

const styles = StyleSheet.create({
  body: {
    padding: 20,
  },
  image: {
    maxWidth: 100,
  },
});
function PDF() {
  const userLoginData = useSelector(
    (state) => state.userLoginReducer.loginUser,
  );

  return (
    <PDFViewer width="75%" height="750px">
      <Document title="PROFILE">
        <Page style={styles.body}>
          <View style={styles.image}>
            <Image
              src={
                userLoginData[0]?.data?.profile_picture
                  ? userLoginData[0]?.data?.profile_picture
                  : 'https://mir-s3-cdn-cf.behance.net/project_modules/fs/e1fd5442419075.57cc3f77ed8c7.png'
              }
              alt="user profile"
            />
          </View>
          <View>
            <Text style={{ marginTop: '10px' }}>
              Name :
              {' '}
              {userLoginData[0]?.data?.name}
            </Text>
            <Text>
              Username :
              {' '}
              {userLoginData[0]?.data?.username}
            </Text>
            <Text>
              Email :
              {' '}
              {userLoginData[0]?.data?.email}
            </Text>
            <Text>
              City :
              {' '}
              {userLoginData[0]?.data?.city}
            </Text>
            <Text style={{ marginTop: '10px' }}>
              Total Game :
              {' '}
              {userLoginData[0]?.data?.total_game}
            </Text>
            <Text>
              Total Point :
              {' '}
              {userLoginData[0]?.data?.total_score}
            </Text>
            <Text>
              Player Rank :
              {userLoginData[0]?.data?.player_rank}
            </Text>
          </View>
        </Page>
      </Document>
    </PDFViewer>
  );
}

export default PDF;
