import dynamic from 'next/dynamic';

const DynamicDocument = dynamic(() => import('../Pdf'), {
  ssr: false,
});

export default DynamicDocument;
