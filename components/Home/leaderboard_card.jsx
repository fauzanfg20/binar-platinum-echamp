import { Card, Row, Col } from 'react-bootstrap';

function LeaderboardCard(props) {
  const { data } = props;
  return (
    <Card style={{ backgroundColor: '#00000050' }} className="mt-2 p-3">
      <Row>
        <Col xs="auto">
          <img
            src={data.image}
            style={{ width: 60, height: 60, borderRadius: '50%' }}
            alt="data_image"
          />
        </Col>
        <Col>
          <div
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              textAlignVertical: 'center',
            }}
          >
            <h5 className="text-start">{data.name}</h5>
            <span className="text-start">
              SCORE:
              {data.score}
            </span>
          </div>
        </Col>
      </Row>
    </Card>
  );
}

export default LeaderboardCard;
