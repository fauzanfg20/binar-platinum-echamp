import Image from 'next/image';

import btnSlide from '../../public/assets/scroll_down.svg';
import styleSlideShow from './slideshow.module.css';

function Slideshow(props) {
  const style = {
    slideshow: {
      height: 350,
    },
  };

  const { data } = props;
  return (
    <div
      id="carouselExampleIndicators"
      className="carousel slide"
      data-ride="carousel"
    >
      <ol className="carousel-indicators">
        {data.map((request, index) => (index === 0 ? (
          <span
            data-target="#carouselExampleIndicators"
            className={`${styleSlideShow.dot} active`}
            data-slide-to={index}
            key={request.id}
          />
        ) : (
          <span
            data-target="#carouselExampleIndicators"
            className={styleSlideShow.dot}
            data-slide-to={index}
            key={request.id}
          />
        )))}
      </ol>
      <div className="carousel-inner">
        {data.map((request, index) => (index === 0 ? (
          <div
            className="carousel-item active"
            key={request.id}
            style={style.slideshow}
          >
            <img className="d-block w-100" src={request.data.hs_image} alt="hs_image" />
          </div>
        ) : (
          <div
            className="carousel-item"
            key={request.id}
            style={style.slideshow}
          >
            <img className="d-block w-100" src={request.data.hs_image} alt="hs_image" />
          </div>
        )))}
      </div>
      <a
        className={styleSlideShow.carouselControlPrev}
        href="#carouselExampleIndicators"
        role="button"
        data-slide="prev"
      >
        <Image
          width={23}
          height={12}
          src={btnSlide}
          style={{ transform: 'rotate(90deg)' }}
          alt="slideshow"
        />
      </a>
      <a
        className={styleSlideShow.carouselControlNext}
        href="#carouselExampleIndicators"
        role="button"
        data-slide="next"
      >
        <Image
          width={23}
          height={12}
          src={btnSlide}
          alt=""
          style={{ transform: 'rotate(-90deg)' }}
        />
        {' '}
      </a>
    </div>
  );
}

export default Slideshow;
