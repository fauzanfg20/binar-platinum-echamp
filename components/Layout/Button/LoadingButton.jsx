import { Button } from 'react-bootstrap';
import { useSelector } from 'react-redux';

function LoadingButton({ title, onClick, variant }) {
  const loadingData = useSelector((state) => state.loadingReducer);

  if (loadingData.loadingStatus) {
    return (
      <Button variant={variant} disabled type="submit" className="mx-3">Loading</Button>
    );
  }
  return (
    <Button variant={variant} onClick={onClick} type="submit" className="mx-3">{title}</Button>
  );
}

export default LoadingButton;
