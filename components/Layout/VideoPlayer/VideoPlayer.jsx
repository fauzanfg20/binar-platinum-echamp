import React from 'react';
import { Player } from 'video-react';
// import { getVideoInfo } from '../../../actions/fb_database';

function VideoPlayer({ source }) {
  return (
    <Player>
      <source src={source} />
    </Player>
  );
}

export default VideoPlayer;
