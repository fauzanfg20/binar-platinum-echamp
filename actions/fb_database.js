// import { createUserWithEmailAndPassword } from 'firebase/auth';
import {
  set, ref, onValue, update, push,
} from 'firebase/database';
import { authFirebase, database } from '../config/firebase';
// import { retrieveAllGames } from './games';

const db = database;

export const registerUser = async (id_player, name, username, email) => {
  const dbRef = ref(db, `game_user/${id_player}`);
  // console.log('dbRef', dbRef);
  const data = {
    id_player,
    name,
    username,
    email, // isinya email
    total_score: 0,
    city: '',
    social_media: '',
    profile_picture:
      'https://mir-s3-cdn-cf.behance.net/project_modules/fs/e1fd5442419075.57cc3f77ed8c7.png',
    total_game: 0,
    player_rank: 0,
    playedGame: [],
    role: 'player',
  };
  await set(dbRef, data);
};

// get all user
export const retrieveAllUser = () => new Promise((resolve, reject) => {
  const dbRef = ref(db, 'game_user');
  onValue(dbRef, (snapshot) => {
    const value = [];
    // Ubah Object ke Array of Object
    Object.keys(snapshot.val()).map((key) => {
      value.push({
        id: key,
        data: snapshot.val()[key],
      });
    });
    resolve(value);
  });
});

// get game score
export const retrieveAllScore = () => new Promise((resolve, reject) => {
  const dbRef = ref(db, 'game_score');
  onValue(dbRef, (snapshot) => {
    const value = [];
    // Ubah Object ke Array of Object
    Object.keys(snapshot.val()).map((key) => {
      value.push({
        id: key,
        data: snapshot.val()[key],
      });
    });
    resolve(value);
  });
});

// read one biodata
export const getUserById = async (id) => new Promise((resolve, reject) => {
  const dbRef = ref(db, `game_user/${id}`);
  onValue(dbRef, (data) => {
    const value = [];
    value.push({
      id,
      data: data.val(),
    });
    resolve(value);
  });
});

// edit profile
export const updateProfile = (
  id,
  name,
  username,
  city,
  social_media,
  profile_picture,
) => {
  const dbRef = ref(db, `game_user/${id}`);
  const data = {
    name,
    username,
    city,
    social_media,
    profile_picture,
  };
  update(dbRef, data);
};

// update score
export const updateScore = (id, total_score) => {
  const dbRef = ref(db, `game_user/${id}`);
  const data = {
    total_score,
  };
  update(dbRef, data);
};

// update total game
export const updateTotalGame = (id, total_game) => {
  const dbRef = ref(db, `game_user/${id}`);
  const data = {
    total_game,
  };
  update(dbRef, data);
};

// Update played game
export const updatePlayedGame = (id, playedGame) => {
  const dbRef = ref(db, `game_user/${id}`);
  const data = {
    playedGame,
  };
  update(dbRef, data);
};

// update player rank
export const updatePlayerRank = (id, player_rank) => {
  const dbRef = ref(db, `game_user/${id}`);
  const data = {
    player_rank,
  };
  update(dbRef, data);
};

// update profile photo
export const updateProfileImg = (id, profile_picture) => {
  const dbRef = ref(db, `game_user/${id}`);
  const data = {
    profile_picture,
  };
  update(dbRef, data);
};
// get game info
export const getGameInfoById = (id) => new Promise((resolve, reject) => {
  const dbRef = ref(db, `game_info/${id}`);
  onValue(dbRef, (data) => {
    const value = data.val();
    resolve(value);
  });
});
// total point
export const totalPointByUser = async (id) => {
  const store = [];
  let point = 0;
  const scoreAll = await retrieveAllScore();
  scoreAll.forEach((e) => {
    if (e.data.id_player == id) {
      store.push(e);
    }
  });
  store.forEach((e) => {
    point += e.data.score;
  });
  const id_generate = await getUserById(id);
  await updateScore(id_generate[0].id, point);

  return point;
};
// game history
export const historyByUser = async (id) => {
  const store = [];
  const scoreAll = await retrieveAllScore();
  scoreAll.forEach((e) => {
    if (e.data.id_player == id) {
      store.push(e);
    }
  });

  return store;
};
// leaderboard pergame
export const leaderBoardByGame = async (id) => {
  const temp = [];
  const result = [];

  const scoreAll = await retrieveAllScore();
  scoreAll.forEach((e) => {
    if (e.data.game_id == id) {
      temp.push(e);
    }
  });

  temp.reduce((res, value) => {
    if (!res[value.data.id_player]) {
      res[value.data.id_player] = { id_player: value.data.id_player, score: 0 };
      result.push(res[value.data.id_player]);
    }
    res[value.data.id_player].score += value.data.score;
    return res;
  }, {});
  // const sorted = Object.keys(resultan).sort(function(a,b){return resultan[a]-resultan(b)})
  const tempSort = result.sort((a, b) => b.score - a.score);

  return tempSort;
};
// jumlah user yang bermain pergame
export const countPlayerByGame = async (id) => {
  let counter = 0;
  const player = [];
  const game_player = [];
  const scoreAll = await retrieveAllScore();
  scoreAll.forEach((e) => {
    if (e.data.game_id == id) {
      player.push(e);
    }
  });
  player.forEach((e) => {
    if (game_player.includes(e.data.id_player) === false) {
      game_player.push(e.data.id_player);
      counter += 1;
    }
  });
  return counter;
};

// player rank
export const playerRank = async (id) => {
  const result = [];
  const scoreAll = await retrieveAllScore();
  scoreAll.reduce((res, value) => {
    if (!res[value.data.id_player]) {
      res[value.data.id_player] = { id_player: value.data.id_player, score: 0 };
      result.push(res[value.data.id_player]);
    }
    res[value.data.id_player].score += value.data.score;
    return res;
  }, {});
  const tempSort = result.sort((a, b) => b.score - a.score);
  const rank = tempSort.findIndex((x) => x.id_player === id);
  updatePlayerRank(id, rank + 1);
  return rank + 1;
};

// total game per user
export const totalGameByUser = async (id) => {
  const game_list = [];
  const temp = [];
  const scoreAll = await retrieveAllScore();
  scoreAll.forEach((e) => {
    if (e.data.id_player == id) {
      temp.push(e);
    }
  });
  temp.forEach((e) => {
    if (game_list.includes(e.data.game_id) === false) {
      game_list.push(e.data.game_id);
    }
  });
  updateTotalGame(id, game_list.length);
  return game_list.length;
};

// played game
export const playedGame = async (id) => {
  const game_list = [];
  const temp = [];
  const scoreAll = await retrieveAllScore();
  scoreAll.forEach((e) => {
    if (e.data.id_player == id) {
      temp.push(e);
    }
  });
  temp.forEach((e) => {
    if (game_list.includes(e.data.game_id) === false) {
      game_list.push(e.data.game_id);
    }
  });

  updatePlayedGame(id, game_list);
};

// insertHomeGameCard
export const insertGameCard = async (
  game_title,
  game_description,
  game_image,
  game_url,
) => {
  const dbRef = ref(db, 'game_info');
  const data = {
    game_title,
    game_description,
    game_image,
    game_url,
  };
  await push(dbRef, data);
};

// insertSlideHomeShow
export const insertSlideshow = async (hs_image, hs_title) => {
  const dbRef = ref(db, 'home_slideshow');
  const data = {
    hs_image,
    hs_title,
  };
  await push(dbRef, data);
};

// insertHomeVideo
export const insertVideo = async (url_video, video_title) => {
  const dbRef = ref(db, 'home_video/vid');
  const data = {
    url_video,
    video_title,
  };
  await set(dbRef, data);
};

export const getVideoInfo = () => new Promise((resolve, reject) => {
  const dbRef = ref(db, 'home_video/vid');
  onValue(dbRef, (data) => {
    const value = data.val();
    resolve(value);
  });
});
