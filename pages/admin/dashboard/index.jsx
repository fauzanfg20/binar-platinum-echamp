import React, { useState, useEffect } from 'react';
import Form from 'react-bootstrap/Form';
import Image from 'react-bootstrap/Image';
import { toast, ToastContainer } from 'react-toastify';

import { useSelector, useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import {
  insertGameCard,
  insertSlideshow,
  insertVideo,
} from '../../../actions/fb_database';
import {
  uploadImgCloudinary,
  uploadVideoCloudinary,
} from '../../../actions/fb_storage';
import LoadingButton from '../../../components/Layout/Button/LoadingButton';

import Navbar from '../../../components/Layout/Nav/Navbar';
import style from '../../../styles/admin/dashboard.module.css';
import { loadingAction } from '../../../redux/reducers/loadingReducer';

export default function AdminDashboard() {
  const [gameInfoData, setGameInfoData] = useState({
    gameDescription: '',
    gameTitle: '',
    gameUrl: '',
  });
  const dispatch = useDispatch();
  const router = useRouter();
  const userLoginData = useSelector(
    (state) => state.userLoginReducer.loginUser,
  );

  const [gameInfoImage, setGameInfoImage] = useState('');
  const [gameInfoImageTempUrl, setGameInfoImageTempUrl] = useState('');

  const [homeSlideShowData, setHomeSlideShowData] = useState({
    homeSlideShowImgTitle: '',
  });
  const [homeSlideShowImage, setHomeSlideShowImage] = useState('');
  const [homeSlideShowImageTempUrl, setHomeSlideShowImageTempUrl] = useState('');

  const [homeVideoData, setHomeVideoData] = useState({
    videoTitle: '',
  });

  const [homeVideoFile, setHomeVideoFile] = useState('');
  const [homeVideoFileTempUrl, setHomeVideoFileTempUrl] = useState('');
  // admin check
  const checker = () => {
    // console.log('login data', userLoginData[0]?.data?.role);
    if (userLoginData) {
      if (userLoginData[0]?.data?.role === 'player') {
        router.push('/');
      }
    }
  };
  // Submit Data
  const gameInfoSubmitHandler = async (event) => {
    dispatch(loadingAction.toggleLoadingStatus());
    event.preventDefault();
    const urlCloudinary = await uploadImgCloudinary(gameInfoImage);
    await insertGameCard(
      gameInfoData.gameTitle,
      gameInfoData.gameDescription,
      urlCloudinary,
      gameInfoData.gameUrl,
    );

    setGameInfoData({
      gameDescription: '',
      gameTitle: '',
      gameUrl: '',
    });

    setHomeSlideShowImage('');

    setGameInfoImageTempUrl(
      'https://img.freepik.com/free-vector/illustration-gallery-icon_53876-27002.jpg?w=1380&t=st=1671185246~exp=1671185846~hmac=deff28779bb42c42d7fa7aeb02f55b198c18fe335d617663a73fbd1dd4b4f0e5',
    );
    dispatch(loadingAction.toggleLoadingStatus());
    toast.success('Insert Game Info Successfully');
  };

  const homeSlideShowSubmitHandler = async (event) => {
    dispatch(loadingAction.toggleLoadingStatus());
    event.preventDefault();
    const urlCloudinary = await uploadImgCloudinary(homeSlideShowImage);
    await insertSlideshow(
      urlCloudinary,
      homeSlideShowData.homeSlideShowImgTitle,
    );

    setHomeSlideShowData({ homeSlideShowImgTitle: '' });

    setHomeSlideShowImage('');

    setHomeSlideShowImageTempUrl(
      'https://img.freepik.com/free-vector/illustration-gallery-icon_53876-27002.jpg?w=1380&t=st=1671185246~exp=1671185846~hmac=deff28779bb42c42d7fa7aeb02f55b198c18fe335d617663a73fbd1dd4b4f0e5',
    );
    dispatch(loadingAction.toggleLoadingStatus());
    toast.success('Insert HomeSlideShow Image Successfully');
  };

  const homeVideoSubmitHandler = async (event) => {
    dispatch(loadingAction.toggleLoadingStatus());
    event.preventDefault();
    const urlCloudinary = await uploadVideoCloudinary(homeVideoFile);
    await insertVideo(urlCloudinary, homeVideoData.videoTitle);

    setHomeVideoData({
      videoTitle: '',
    });
    dispatch(loadingAction.toggleLoadingStatus());
    toast.success('Insert HomeSlideShow Video Successfully');
  };

  // File Change Handler
  const gameInfoImageFileChangeHandler = (event) => {
    if (event.target.files[0]) {
      const reader = new FileReader();
      setGameInfoImage(event.target.files[0]);
      reader.onload = () => {
        if (reader.readyState === 2) {
          setGameInfoImageTempUrl(reader.result);
        }
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  };

  const homeSlideShowImageFileChangeHandler = (event) => {
    if (event.target.files[0]) {
      const reader = new FileReader();
      setHomeSlideShowImage(event.target.files[0]);
      reader.onload = () => {
        if (reader.readyState === 2) {
          setHomeSlideShowImageTempUrl(reader.result);
        }
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  };

  const homeVideoFileChangeHandler = (event) => {
    if (event.target.files[0]) {
      const reader = new FileReader();
      setHomeVideoFile(event.target.files[0]);
      reader.onload = () => {
        if (reader.readyState === 2) {
          setHomeVideoFileTempUrl(reader.result);
        }
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  };

  // Data Change Handler
  const gameInfoChangeHandler = (event) => {
    const { name, value } = event.target;
    setGameInfoData((val) => ({
      ...val,
      [name]: value,
    }));
  };

  const homeSlideShowChangeHandler = (event) => {
    const { name, value } = event.target;
    setHomeSlideShowData((val) => ({
      ...val,
      [name]: value,
    }));
  };

  const homeVideoChangeHandler = (event) => {
    const { name, value } = event.target;
    setHomeVideoData((val) => ({
      ...val,
      [name]: value,
    }));
  };

  useEffect(() => {
    checker();
  }, [userLoginData]);

  return (
    <>
      <ToastContainer />
      <Navbar />
      <div
        className={`container ${style.adminDashboardParent}`}
        style={{ width: '100%' }}
      >
        <div
          className="accordion"
          id="accordionExample"
          style={{ width: '100%' }}
        >
          {/* Insert GameInfo */}
          <div className="accordion-item">
            <h2 className="accordion-header" id="headingOne">
              <button
                className="accordion-button"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#collapseOne"
                aria-expanded="true"
                aria-controls="collapseOne"
              >
                Insert GameInfo
              </button>
            </h2>
            <div
              id="collapseOne"
              className="accordion-collapse collapse show"
              aria-labelledby="headingOne"
              data-bs-parent="#accordionExample"
              style={{ width: '100%' }}
            >
              <div className="accordion-body">
                <Form onSubmit={gameInfoSubmitHandler}>
                  <Form.Group className="mb-3" controlId="formGameDescription">
                    <Form.Label>Game Description</Form.Label>
                    <Form.Control
                      name="gameDescription"
                      type="text"
                      placeholder="Game Description"
                      value={gameInfoData.gameDescription}
                      onChange={gameInfoChangeHandler}
                    />
                  </Form.Group>

                  <Form.Group controlId="formGameImage" className="mb-3">
                    <Form.Label>Game Image</Form.Label>
                    <Form.Control
                      type="file"
                      onChange={gameInfoImageFileChangeHandler}
                    />
                  </Form.Group>

                  <Image
                    width="100"
                    src={
                      gameInfoImageTempUrl
                      || 'https://img.freepik.com/free-vector/illustration-gallery-icon_53876-27002.jpg?w=1380&t=st=1671185246~exp=1671185846~hmac=deff28779bb42c42d7fa7aeb02f55b198c18fe335d617663a73fbd1dd4b4f0e5'
                    }
                    alt=""
                  />

                  <Form.Group className="mb-3" controlId="formGameTitle">
                    <Form.Label>Game Title</Form.Label>
                    <Form.Control
                      name="gameTitle"
                      type="text"
                      placeholder="Game Title"
                      value={gameInfoData.gameTitle}
                      onChange={gameInfoChangeHandler}
                    />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="formGameUrl">
                    <Form.Label>Game Url</Form.Label>
                    <Form.Control
                      name="gameUrl"
                      type="text"
                      placeholder="Game Url"
                      value={gameInfoData.gameUrl}
                      onChange={gameInfoChangeHandler}
                    />
                  </Form.Group>

                  {/* <Button variant="primary" type="submit">
                    Submit
                  </Button> */}
                  <LoadingButton title="submit" />
                </Form>
              </div>
            </div>

            {/* Insert Home SlideShow */}
            <div className="accordion-item">
              <h2 className="accordion-header" id="headingTwo">
                <button
                  className="accordion-button collapsed"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#collapseTwo"
                  aria-expanded="false"
                  aria-controls="collapseTwo"
                >
                  Insert Home Slideshow
                </button>
              </h2>
              <div
                id="collapseTwo"
                className="accordion-collapse collapse"
                aria-labelledby="headingTwo"
                data-bs-parent="#accordionExample"
              >
                <div className="accordion-body">
                  <Form onSubmit={homeSlideShowSubmitHandler}>
                    <Form.Group
                      controlId="formHomeSlideshowImage"
                      className="mb-3"
                    >
                      <Form.Label>Home Slideshow Image</Form.Label>
                      <Form.Control
                        type="file"
                        onChange={homeSlideShowImageFileChangeHandler}
                      />
                    </Form.Group>

                    <Image
                      width="100"
                      src={
                        homeSlideShowImageTempUrl
                        || 'https://img.freepik.com/free-vector/illustration-gallery-icon_53876-27002.jpg?w=1380&t=st=1671185246~exp=1671185846~hmac=deff28779bb42c42d7fa7aeb02f55b198c18fe335d617663a73fbd1dd4b4f0e5'
                      }
                      alt=""
                    />

                    <Form.Group
                      className="mb-3"
                      controlId="formHomeSlideshowImgTitle"
                    >
                      <Form.Label>Home Slideshow Image Title</Form.Label>
                      <Form.Control
                        name="homeSlideShowImgTitle"
                        type="text"
                        placeholder="Home Slideshow Image Title"
                        onChange={homeSlideShowChangeHandler}
                      />
                    </Form.Group>

                    {/* <Button variant="primary" type="submit">
                      Submit
                    </Button> */}
                    <LoadingButton title="submit" />
                  </Form>
                </div>
              </div>
            </div>

            {/* Insert Home Video */}
            <div className="accordion-item">
              <h2 className="accordion-header" id="headingThree">
                <button
                  className="accordion-button collapsed"
                  type="button"
                  data-bs-toggle="collapse"
                  data-bs-target="#collapseThree"
                  aria-expanded="false"
                  aria-controls="collapseThree"
                >
                  Insert Home Video
                </button>
              </h2>

              <div
                id="collapseThree"
                className="accordion-collapse collapse"
                aria-labelledby="headingThree"
                data-bs-parent="#accordionExample"
              >
                <div className="accordion-body">
                  <Form onSubmit={homeVideoSubmitHandler}>
                    <Form.Group className="mb-3" controlId="formVideoTitle">
                      <Form.Label>Video Title</Form.Label>
                      <Form.Control
                        name="videoTitle"
                        type="text"
                        placeholder="Video Title"
                        onChange={homeVideoChangeHandler}
                      />
                    </Form.Group>

                    <Form.Group
                      controlId="formHomeSlideshowImage"
                      className="mb-3"
                    >
                      <Form.Label>Video File</Form.Label>
                      <Form.Control
                        type="file"
                        onChange={homeVideoFileChangeHandler}
                      />
                    </Form.Group>

                    {/* <Button variant="primary" type="submit">
                      Submit
                    </Button> */}
                    <LoadingButton title="submit" />
                  </Form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
