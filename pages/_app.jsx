import { useSelector, useDispatch } from 'react-redux';
import { useEffect } from 'react';

import { authFirebase } from '../config/firebase';
import { retrieveLoginUser } from '../redux/reducers/loginReducer';
import 'react-toastify/dist/ReactToastify.css';
import { wrapper } from '../redux/store';
import 'video-react/dist/video-react.css';
import '../styles/globals.css';

function MyApp({ Component, ...rest }) {
  const dispatch = useDispatch();
  const userLoginData = useSelector((state) => state.userLoginReducer.loginUser);
  const { props } = wrapper.useWrappedStore(rest);

  // to check firebase auth state
  useEffect(() => {
    const unsubscribe = authFirebase.onAuthStateChanged(async (user) => {
      if (user) {
        dispatch(retrieveLoginUser(user.uid));
      }
    });
    // cleanup
    return () => unsubscribe();
  }, [dispatch]);

  // console.log(userLoginData);

  return (
    <Component {...props.pageProps} />
  );
}
export default wrapper.withRedux(MyApp);
